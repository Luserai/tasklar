
Functions.java

public class Functions {
    public static int sum(int a, int b){
        return a+b;
    }
    public static int multiply(int a,int b){
        return a*b;
    }

    public static int subtract(int a, int b) { return a-b;}

    public static int divide(int a, int b) { return a/b; }
}



Main.java


import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Functions fun = new Functions();
        Scanner input = new Scanner(System.in);

        System.out.println("Enter firs number");
        int a = input.nextInt();
        System.out.println("Enter second number");
        int b = input.nextInt();

        System.out.println("What to do: +, * ,/, - ");
        String symbol = input.next();

        switch (symbol) {
            case "+":
                System.out.println(fun.sum(a, b));
                break;

            case "*":
                System.out.println(fun.multiply(a, b));
                break;

            case "-":
                System.out.println(fun.subtract(a, b));
                break;

            case "/":
                System.out.println(fun.divide(a, b));
                break;

        }
    }
}
