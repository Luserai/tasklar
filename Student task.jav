
Main.java

public class Main {
    public static void main(String[] args) {
        Student student = new Student();

        student.setName("Rasul");
        student.setAge(20);
        student.setGrade(11);

        student.Information();
    }
}



Student.java

public class Student {
    private String name;
    private int age;
    private int grade;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getGrade() {
        return grade;
    }

    public void Information() {
        System.out.println("Student Name: " + name);
        System.out.println("Age: " + age);
        System.out.println("Grade: " + grade);
    }
}
