abstract class Vehicle {
    abstract void startEngine();
    abstract void drive();
}

class Car extends Vehicle {
    @Override
    void startEngine() {
        System.out.println("Car engine started.");
    }

    @Override
    void drive() {
        System.out.println("Car is being driven.");
    }
}

class Motorcycle extends Vehicle {
    @Override
    void startEngine() {
        System.out.println("Motorcycle engine started.");
    }

    @Override
    void drive() {
        System.out.println("Motorcycle is being driven.");
    }
}

class Truck extends Vehicle {
    @Override
    void startEngine() {
        System.out.println("Truck engine started.");
    }

    @Override
    void drive() {
        System.out.println("Truck is being driven.");
    }
}

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        car.startEngine();
        car.drive();

        Motorcycle motorcycle = new Motorcycle();
        motorcycle.startEngine();
        motorcycle.drive();

        Truck truck = new Truck();
        truck.startEngine();
        truck.drive();
    }
}
